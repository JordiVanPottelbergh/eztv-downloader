CREATE TABLE watching(
	showId INTEGER PRIMARY KEY AUTOINCREMENT ,
	showName varchar(50) NOT NULL,
	siteId number(4) NOT NULL,
	episodeNumber varchar(50) NOT NULL
);

CREATE TABLE errors(
        showName VARCHAR(100),
        episodeName VARCHAR(100),
        magnetURI VARCHAR(500),
        checked INTEGER(1)
);