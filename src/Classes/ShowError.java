package Classes;

public class ShowError {
	/*
	 * showName VARCHAR(100), 
	 * episodeName VARCHAR(100), 
	 * magnetURI VARCHAR(500),
	 * checked INTEGER(1)
	 */
	private String showName;
	private String episodeName;
	private String magnetURI;
	private boolean checked;

	public ShowError(String showName, String episodeName, String magnetURI,
			boolean checked) {
		super();
		this.showName = showName;
		this.episodeName = episodeName;
		this.magnetURI = magnetURI;
		this.checked = checked;
	}

	public String getShowName() {
		return showName;
	}

	public String getEpisodeName() {
		return episodeName;
	}

	public String getMagnetURI() {
		return magnetURI;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public void setEpisodeName(String episodeName) {
		this.episodeName = episodeName;
	}

	public void setMagnetURI(String magnetURI) {
		this.magnetURI = magnetURI;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
