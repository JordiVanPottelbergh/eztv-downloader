package Classes;

public class watch implements Comparable<watch> {
	private int id;
	private String name;
	private int siteId;
	private String episodeNumber;
	private boolean hasEnded;
	
	private String url;

	private static String base = "http://eztv.it/shows/";
	
	public String toString() {
		return name;
	}
	
	public watch(int id, String name, int siteId, String episodeNumber) {
		super();
		this.id = id;
		this.name = name;
		this.url = base + siteId + "/";
		this.siteId = siteId;
		this.episodeNumber = episodeNumber;
		hasEnded = false;
	}
	
	public boolean hasEnded() {
		return hasEnded;
	}

	public int getId() {
		return id;
	}
	
	public static String getBase() {
		return base;
	}

	public int getSiteId() {
		return siteId;
	}
	
	public String getEpisodeNumber() {
		return episodeNumber;
	}

	public String getName() {
		return name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setEpisodeNumber(String episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	
	public void setHasEnded(boolean hasEnded) {
		this.hasEnded = hasEnded;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + siteId;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		watch other = (watch) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (siteId != other.siteId)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public int compareTo(watch o) {
		return this.getName().toLowerCase().compareTo(o.getName().toLowerCase());
	}
	
}
