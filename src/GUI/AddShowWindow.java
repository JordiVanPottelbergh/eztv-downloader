package GUI;

import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import webCrawler.crawlWeb;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import GUI.models.ShowTableModel;
import GUI.models.folderChooser;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class AddShowWindow extends JFrame {
	private static final long serialVersionUID = 7956425269364379826L;
	
	private JPanel contentPane;
	private TreeMap<Integer, String> showList = crawlWeb.getAllShows();
	private JComboBox cbxShowName;
	
	//search variables
	private JTextField txtSearch;
	private final JScrollPane scpSearch = new JScrollPane();
	private JTable tblSearchList;
	private ShowTableModel showTblModel;
	
	/**
	 * Create the frame.
	 */
	public AddShowWindow(final Main main) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 546, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblShowName = new JLabel("Show Name");
		lblShowName.setBounds(10, 11, 107, 14);
		contentPane.add(lblShowName);
		
		cbxShowName = new JComboBox(new DefaultComboBoxModel());
		cbxShowName.addItem("Selecteer");
		for(int x : showList.keySet()) cbxShowName.addItem(showList.get(x));
		cbxShowName.setBounds(120, 10, 400, 17);
		contentPane.add(cbxShowName);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cbxShowName.getSelectedIndex() > 0) {
					for(int x : showList.keySet()) {
						if(showList.get(x).equals(cbxShowName.getSelectedItem())) {
							main.addShow(showList.get(x), x);
							dispose();
							return;
						}
					}
				} else if (tblSearchList.getSelectedRows().length > 0) {
					int selectedRows[] = tblSearchList.getSelectedRows();
					
					for(int i = 0; i < selectedRows.length; i++) {
						main.addShow((String) showTblModel.getValueAt(selectedRows[i], 1), (int) showTblModel.getValueAt(selectedRows[i], 0));
					}
					
					dispose();
					return;
				}
			}
		});
		btnAdd.setBounds(0, 427, 89, 23);
		contentPane.add(btnAdd);
		
		JLabel lblOr = new JLabel("OR");
		lblOr.setHorizontalAlignment(SwingConstants.CENTER);
		lblOr.setBounds(147, 26, 259, 32);
		contentPane.add(lblOr);
		
		JLabel lblSearch = new JLabel("Show Name");
		lblSearch.setBounds(10, 71, 107, 14);
		contentPane.add(lblSearch);
		
		txtSearch = new JTextField();
		txtSearch.setBounds(120, 69, 310, 17);
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);
		scpSearch.setBounds(0, 96, 530, 320);
		contentPane.add(scpSearch);
		
		showTblModel = new ShowTableModel(new TreeMap<Integer,String>());
		tblSearchList = new JTable(showTblModel);
		scpSearch.setViewportView(tblSearchList);
		
		JButton btnZoek = new JButton("Zoek");
		btnZoek.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!txtSearch.getText().equals("")) {
					showTblModel = new ShowTableModel(crawlWeb.getShowBySearch(txtSearch.getText()));
					tblSearchList = new JTable(showTblModel);
					scpSearch.setViewportView(tblSearchList);

				}
			}
		});
		btnZoek.setBounds(441, 67, 89, 23);
		contentPane.add(btnZoek);
		
		JButton btnImportFolder = new JButton("Import Folder");
		btnImportFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				chooser.setFileFilter(new folderChooser());
				chooser.setDialogTitle("Select Folder to Loop Through");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				
				if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File selectedFile = chooser.getSelectedFile();
					
					System.out.println(selectedFile.getName());
					
					for(int i = 0; i < selectedFile.listFiles().length; i++) {
						if(selectedFile.listFiles()[i].isDirectory()) {
							String name = selectedFile.listFiles()[i].getName().toLowerCase();
							
							TreeMap<Integer, String> map = crawlWeb.getShowBySearch(name);
							System.out.println(map.size() + " => " + name);
							
							/*ddd apartment ddd
							ddd apartment
							apartment ddd
							apartement*/
							
							Pattern matchExactFront = Pattern.compile("^" + name + ".*?$");
							Pattern matchNotExact = Pattern.compile("^.*?" + name + ".*?$");
							Pattern matchExactBack = Pattern.compile("^.*?" + name + "$");
														
							for(int me : map.keySet()) {
								String s = map.get(me).toLowerCase();
								
								boolean handleMe = false;
								
								if(s.equals(name)) {
									handleMe = true;
								} else {
									Matcher m_matchExactFront = matchExactFront.matcher(s);
									
									if(m_matchExactFront.matches()) {
										handleMe = true;
									} else {
										Matcher m_matchNotExact = matchNotExact.matcher(s);
										
										if(m_matchNotExact.matches()) {
											handleMe = true;
										} else {
											Matcher m_matchExactBack = matchExactBack.matcher(s);
											
											if(m_matchExactBack.matches()) {
												handleMe = true;
											}
										}
									}
								}
								
								if(handleMe) {
									System.out.println("Handling Show " + s);
									main.addShow(s, me);
								}
							}
						}
					}
				}
			}
		});
		btnImportFolder.setBounds(393, 427, 127, 23);
		contentPane.add(btnImportFolder);
		this.setVisible(true);
	}
}
