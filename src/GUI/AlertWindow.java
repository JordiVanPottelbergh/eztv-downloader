package GUI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class AlertWindow {
	private JFrame frmAlertWindow;
	//private JTextArea txtErrorMessage;
	private JLabel lblStatus;
		
	public AlertWindow(String msg) {
		int lines = msg.split("\r\n|\r|\n").length;
		int txtHeight = 14 + (15 * lines);
		int frmHeight = (lines >= 5) ? txtHeight + 111 : 170;
		int btnY = frmHeight - 72;
		int statusY = btnY - 20;
		
		frmAlertWindow = new JFrame();
		frmAlertWindow.setBounds(100, 100, 500, frmHeight);
		frmAlertWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAlertWindow.getContentPane().setLayout(null);
		frmAlertWindow.setLocationRelativeTo(null);
		frmAlertWindow.requestFocus();
		
		JTextArea txtErrorMessage = new JTextArea();
		txtErrorMessage.setText(msg);
		txtErrorMessage.setBackground(null);
		txtErrorMessage.setFont(new Font("Arial", Font.PLAIN, 13));
		txtErrorMessage.setLineWrap(true);
		txtErrorMessage.setWrapStyleWord(true);
		txtErrorMessage.setEditable(false);
		txtErrorMessage.setBounds(10, 11, 464, txtHeight);
		frmAlertWindow.getContentPane().add(txtErrorMessage);
		
		JButton btnErrorWindowOk = new JButton("Ok");
		btnErrorWindowOk.setBounds(197, btnY, 89, 23);
		frmAlertWindow.getContentPane().add(btnErrorWindowOk);
		
		lblStatus = new JLabel("");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatus.setBounds(10, statusY, 464,23);
		frmAlertWindow.getContentPane().add(lblStatus);
		
		frmAlertWindow.repaint();
		frmAlertWindow.setVisible(true);
		
		btnErrorWindowOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
	
	public void setStatusText(String msg) {
		this.lblStatus.setText(msg);
	}
	
	public void dispose() {
		frmAlertWindow.dispose();
	}
}
