package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

import database.DAOwatch;

import Classes.watch;

public class EditShowWindow extends JFrame {
	private static final long serialVersionUID = -7057773719973900731L;

	private JPanel contentPane;
	private JTextField txtEpisodenumber;
	private watch mee;
	
	/**
	 * Create the frame.
	 */
	public EditShowWindow(final Main main, watch me) {
		mee = me;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblShowname = new JLabel(me.getName());
		lblShowname.setHorizontalAlignment(SwingConstants.CENTER);
		lblShowname.setBounds(10, 11, 414, 23);
		contentPane.add(lblShowname);
		
		JLabel lblUrl = new JLabel(me.getUrl());
		lblUrl.setHorizontalAlignment(SwingConstants.CENTER);
		lblUrl.setBounds(10, 74, 414, 23);
		contentPane.add(lblUrl);
		
		txtEpisodenumber = new JTextField();
		txtEpisodenumber.setText(me.getEpisodeNumber());
		txtEpisodenumber.setBounds(10, 134, 414, 23);
		contentPane.add(txtEpisodenumber);
		txtEpisodenumber.setColumns(10);
		
		JButton btnChangeEpisodeNumber = new JButton("Change Episode Number");
		btnChangeEpisodeNumber.setBounds(137, 228, 187, 23);
		btnChangeEpisodeNumber.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String job = txtEpisodenumber.getText().toLowerCase();
				
				Pattern correctFormat = Pattern.compile("^(\\d{1,2})x(\\d{1,2})$");
				Matcher m_corFormat = correctFormat.matcher(job);
				
				if(m_corFormat.matches()) {
					int season = Integer.parseInt(m_corFormat.group(1));
					int episode = Integer.parseInt(m_corFormat.group(2));
					
					mee.setEpisodeNumber((season <= 9 ? "0" : "") + season + "x" + (episode <= 9 ? "0" : "") + episode);
					mee.setHasEnded(false);
					
					DAOwatch.saveWatch(mee);
					
					main.getWatchedTblModel().rowsUpdated();
					main.repaint();
					
					((JFrame)((JButton)e.getSource()).getTopLevelAncestor()).dispose();
				}
			}
		});
		contentPane.add(btnChangeEpisodeNumber);
		
		setVisible(true);
	}
}
