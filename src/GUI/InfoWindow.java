package GUI;

import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JButton;

import webCrawler.crawlWeb;

import Classes.watch;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import javax.swing.JScrollPane;

public class InfoWindow extends JFrame {
	private static final long serialVersionUID = -8606022533137190677L;
	private JPanel contentPane;

	private HashMap<String, String> showInfo;

	/**
	 * Create the frame.
	 */
	public InfoWindow(watch show) {
		setTitle("Information about " + show.getName());
		showInfo = crawlWeb.getShowInfo(show.getUrl());

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scpInfo = new JScrollPane();
		contentPane.add(scpInfo, BorderLayout.CENTER);

		JTextPane txtpnInfo = new JTextPane();
		scpInfo.setViewportView(txtpnInfo);

		txtpnInfo.setContentType("text/html");
				
		txtpnInfo.setText("<img src='" + showInfo.get("pic") + "'/><br/>"
				+ "<img src='" + showInfo.get("banner") + "'/><br/>"
				+ showInfo.get("text"));

		txtpnInfo.setCaretPosition(0);
		txtpnInfo.setEditable(false);

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPane.add(btnClose, BorderLayout.SOUTH);

		setVisible(true);
	}

	public void fetchImage(String url, String outputName) {
		ByteArrayOutputStream bais = new ByteArrayOutputStream();
		
		InputStream is = null;
		URL imgUrl = null;
		OutputStream outputStream = null;
		
		try {
			outputStream = new FileOutputStream (outputName);
			imgUrl = new URL(url);

			is = imgUrl.openStream();
			byte[] byteChunk = new byte[4096];
			
			int n;

			while ((n = is.read(byteChunk)) > 0)
				bais.write(byteChunk, 0, n);
			
			bais.writeTo(outputStream);
		} catch (IOException e) {
			System.err.printf("Failed while fetching img " + url);
			e.printStackTrace();

		} finally {
			try {
				if (is != null)
					is.close();
				
				if(outputStream != null)
					outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
	public int getImgWidth(String url) {
		ImageIcon img = new ImageIcon(url);
		
		return img.getIconWidth();
	}

}
