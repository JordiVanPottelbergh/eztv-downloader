package GUI;

/*
 * show howmany shows updated after fetch update
 */

import javax.swing.JFrame;

import java.awt.AWTException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTable;
import javax.swing.JScrollPane;

import database.DAOerrors;
import database.DAOwatch;
import database.Settings;
import Classes.ShowError;
import Classes.watch;

import GUI.models.episodeListTableModel;
import GUI.models.errorListTableModel;
import GUI.models.watchedTableModel;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JComboBox;

import webCrawler.crawlWeb;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JCheckBox;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Main {
	private Main window;
	
	private JFrame frmMain;
	private JTable tblSeriesWatched;
	private JTable tblEpisodeList;
	private JComboBox cbxShowSelect;
	private JScrollPane scpEpisodeList;
	private JScrollPane scpSeriesWatched;
	private JScrollPane scpErrorList;
	
	private episodeListTableModel episodeTblModel;
	private watchedTableModel watchedTblModel;
	private errorListTableModel errorTblModel;
	
	private JCheckBox chckbxShowChecked;
	private JTextField txtTorrentExe;
	private JLabel lblShowcount;
	
	private AlertWindow updateWindow = null;
	
	private int runningShows = 0;
	private int allShowscount = 0;
	
	public static Settings settings = new Settings();
	private JTable tblErrorList;

	
	public static void main(String[] args) throws AWTException {
	    new Main();
	}
	
	public Main() {
		window = this;
		
		frmMain = new JFrame("SeriesChecker");
		frmMain.setBounds(100, 100, 888, 694);
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMain.getContentPane().setLayout(null);
		
		JTabbedPane tpnProgram = new JTabbedPane(JTabbedPane.TOP);
		tpnProgram.setBounds(0, 0, 872, 656);
		frmMain.getContentPane().add(tpnProgram);
		
		JPanel pnlWatched = new JPanel();
		tpnProgram.addTab("Tracked Shows", null, pnlWatched, null);
		pnlWatched.setLayout(null);
		
		scpSeriesWatched = new JScrollPane();
		scpSeriesWatched.setBounds(0, 0, 867, 582);
		pnlWatched.add(scpSeriesWatched);
		
		TreeSet<watch> allShows = DAOwatch.getWatch(-1, "", "");
		for(watch me : allShows)
			if(! me.hasEnded())
				runningShows++;
		setAllShowscount(allShows.size());
		
		watchedTblModel = new watchedTableModel(allShows);
		tblSeriesWatched = new JTable(watchedTblModel);
		tblSeriesWatched.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2 && ((JTable)e.getComponent()).getSelectedRow() != -1) {
					new InfoWindow((((watchedTableModel) tblSeriesWatched.getModel()).getRow(((JTable)e.getComponent()).getSelectedRow())));
				}
			}
		});
		scpSeriesWatched.setViewportView(tblSeriesWatched);
		
		JButton btnAddShow = new JButton("Add Show");
		btnAddShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AddShowWindow(window);
			}
		});
		btnAddShow.setBounds(0, 605, 114, 23);
		pnlWatched.add(btnAddShow);
		
		JButton btnRemoveShow = new JButton("Remove Show");
		btnRemoveShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRows[] = tblSeriesWatched.getSelectedRows();
				
				for(int i = selectedRows.length-1; i > -1; i--) {
					delShow(selectedRows[i]);
				}
			}
		});
		btnRemoveShow.setBounds(248, 605, 130, 23);
		pnlWatched.add(btnRemoveShow);

		JButton btnUpdate = new JButton("Fetch Updates");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						TreeSet<watch> allWatched = DAOwatch.getWatch(-1, "","");
						
						int i = 1;
						for (watch me : allWatched) {						
							if(updateWindow != null)
								updateWindow.setStatusText("Handling " + me.getName() + ", " + i + "/" + allWatched.size());
							
							i++;
							
							if(me.hasEnded())
								continue;
							
							TreeMap<String, String> lastEp = crawlWeb.getLastEpisode(me.getUrl(), -1);
							
							if(lastEp.firstKey().equals("ENDED")) {
								lastEp.values().remove("ENDED");
								me.setHasEnded(true);
							}
							
							// Really last episode
							// [0] == Season
							// [1] == Episode
							int latestNrs[] = new int[2];

							// Latest episode in OUR collection
							int currentNrs[] = new int[2];

							int x = 0;
							for (String s : me.getEpisodeNumber().split("x")) {
								currentNrs[x] = Integer.parseInt(s);
								x++;
							}
							
							x = 0;
							for (String s : lastEp.firstKey().split("x")) {
								latestNrs[x] = Integer.parseInt(s);
								x++;
							}
							
							if ((latestNrs[1] > currentNrs[1] && latestNrs[0] == currentNrs[0])
									|| latestNrs[0] > currentNrs[0]) {
								int myNrs[] = new int[2];
								int highestNrs[] = { 0, 0 };
								
								for (String s : lastEp.keySet()) {
									x = 0;
									
									for (String se : s.split("x")) {
										myNrs[x] = Integer.parseInt(se);
										x++;
									}
									
									if ((myNrs[1] > currentNrs[1] && myNrs[0] == currentNrs[0])
											|| myNrs[0] > currentNrs[0]) {
										// System.out.println(me.getName() +
										// " => " + myNrs[0] + "x" + myNrs[1] +
										// " => " + currentNrs[0] + "x" +
										// currentNrs[1] + " LINK: " +
										// lastEp.get(s));

										String toExec = "\""
												+ settings.getUtorrentPath()
												+ "\" \"" + lastEp.get(s)
												+ "\"";
										
										System.out.println(settings.getUtorrentPath());
										

										try {
											Runtime.getRuntime().exec(toExec);

										} catch (IOException e1) {
											e1.printStackTrace();
										}

										if (highestNrs[0] < myNrs[0]) {
											highestNrs[0] = myNrs[0];
											highestNrs[1] = myNrs[1];
										}

										if (highestNrs[0] == myNrs[0]
												&& highestNrs[1] < myNrs[1]) {
											highestNrs[0] = myNrs[0];
											highestNrs[1] = myNrs[1];
										}
									}
								}

								me.setEpisodeNumber((highestNrs[0] <= 9 ? "0"
										: "")
										+ highestNrs[0]
										+ "x"
										+ (highestNrs[1] <= 9 ? "0" : "")
										+ highestNrs[1]);
								DAOwatch.saveWatch(me);
							}
							
							if(me.hasEnded())
								DAOwatch.saveWatch(me);
						}

						watchedTblModel = new watchedTableModel(DAOwatch
								.getWatch(-1, "", ""));
						tblSeriesWatched = new JTable(watchedTblModel);
						tblSeriesWatched.addMouseListener(new MouseAdapter() {
							public void mouseClicked(MouseEvent e) {
								if (e.getClickCount() == 2
										&& ((JTable) e.getComponent())
												.getSelectedRow() != -1) {
									new InfoWindow(
											(((watchedTableModel) tblSeriesWatched
													.getModel())
													.getRow(((JTable) e
															.getComponent())
															.getSelectedRow())));
								}
							}
						});
						scpSeriesWatched.setViewportView(tblSeriesWatched);
						
						watchedTblModel.showFinished(settings.showFinished());
						
						if(updateWindow != null)
							updateWindow.dispose();
					}
				});
				
				updateWindow = new AlertWindow("Updating Shows in the background.\nDon't start ramming that fetch update button unless" +
						" you want me to fail ;(.");
				
				t.start();

			}
		});
		btnUpdate.setBounds(725, 605, 142, 23);
		pnlWatched.add(btnUpdate);
		
		JButton btnEditShow = new JButton("Edit Show");
		btnEditShow.setBounds(124, 605, 114, 23);
		btnEditShow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(tblSeriesWatched.getSelectedRowCount() == 1)
					new EditShowWindow(window, watchedTblModel.getRow(tblSeriesWatched.getSelectedRow()));
			}
		});
		pnlWatched.add(btnEditShow);
		
		lblShowcount = new JLabel("Number of shows: " + allShowscount + " (Running: " + runningShows + ")");
		lblShowcount.setBounds(4, 588, 370, 14);
		pnlWatched.add(lblShowcount);
		
		JCheckBox checkBox = new JCheckBox("Show Finished");
		checkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				watchedTblModel.showFinished(((JCheckBox)e.getSource()).isSelected());
				settings.setShowFinished(((JCheckBox)e.getSource()).isSelected());
			}
		});
		checkBox.setSelected(settings.showFinished());
		watchedTblModel.showFinished(settings.showFinished());
		checkBox.setBounds(384, 584, 114, 23);
		pnlWatched.add(checkBox);
		
		JPanel pnlEpisodeList = new JPanel();
		tpnProgram.addTab("Show Episode List", null, pnlEpisodeList, null);
		pnlEpisodeList.setLayout(null);
		
		scpEpisodeList = new JScrollPane();
		scpEpisodeList.setBounds(0, 53, 867, 575);
		pnlEpisodeList.add(scpEpisodeList);
		
		tblEpisodeList = new JTable();
		
		cbxShowSelect = new JComboBox(new DefaultComboBoxModel());
		cbxShowSelect.addItem("(Selecteer)");
		for(watch me : DAOwatch.getWatch(-1, "", "")) cbxShowSelect.addItem(me);
		cbxShowSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {			
				if(!cbxShowSelect.getSelectedItem().toString().equals("(Selecteer)")) {
					episodeTblModel = new episodeListTableModel(crawlWeb.getEpisodeMagnetList(((watch) cbxShowSelect.getSelectedItem()).getUrl()));
					tblEpisodeList.setModel(episodeTblModel);
					scpEpisodeList.setViewportView(tblEpisodeList);
				}
			}
		});
		
		cbxShowSelect.setBounds(0, 0, 553, 20);
		pnlEpisodeList.add(cbxShowSelect);
		
		JButton btnDownloadSelected = new JButton("Download Selected");
		btnDownloadSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TreeSet<String> urls = new TreeSet<>( new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						Pattern magnetUri = Pattern.compile("^magnet:\\?(.*?)dn=(.*?)&tr(.*?)$");

						Matcher m_magnetUriO1 = magnetUri.matcher(o1);
						Matcher m_magnetUriO2 = magnetUri.matcher(o2);
																		
						int episode_o1 = 0, episode_o2 = 0;
						int season_o1 = 0, season_o2 = 0;
												
						if (m_magnetUriO1.matches()) {
							int epNrs[] = crawlWeb.parseEpisode(m_magnetUriO1.group(2));
							
							episode_o1 = epNrs[0];
							season_o1 = epNrs[1];

						}
						
						if (m_magnetUriO2.matches()) {
							int epNrs[] = crawlWeb.parseEpisode(m_magnetUriO2.group(2));
							episode_o2 = epNrs[0];
							season_o2 = epNrs[1];
						}
						
						if(season_o1 == season_o2) {
							return episode_o1-episode_o2;
						}
						
						return season_o1-season_o2;
					}
					
				});

				int[] selectedRows = tblEpisodeList.getSelectedRows();
				
				
				for(int x = 0; x < selectedRows.length; x++) {
					urls.add(((episodeListTableModel)tblEpisodeList.getModel()).getUrlAtRow(selectedRows[x]));
				}
				
				System.out.println("\"" + settings.getUtorrentPath() + "\" ");
				
				String base = "\"" + settings.getUtorrentPath() + "\" "; 
				String toExec = base;
				
				ArrayList<String> execMe = new ArrayList<>();
				
				int cur = 0;				
				for(String s : urls) {
					if(cur <= 9) {
						toExec += "\"" + s + "\" ";
						cur++;
					} else {
						toExec += "\"" + s + "\" ";
						execMe.add(toExec);
						
						cur = 0;
						toExec = base;
					}
				}
				
				for(int i = 0; i < execMe.size(); i++) {				
					try {
						Runtime.getRuntime().exec(execMe.get(i));
						
						Thread.sleep(1000);
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (InterruptedException e1) {}
				}
				
				try {
					Runtime.getRuntime().exec(toExec);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnDownloadSelected.setBounds(563, -1, 243, 23);
		pnlEpisodeList.add(btnDownloadSelected);
		
		JPanel pnlShowErrors = new JPanel();
		tpnProgram.addTab("Show Errors", null, pnlShowErrors, null);
		pnlShowErrors.setLayout(null);
		
		scpErrorList = new JScrollPane();
		scpErrorList.setBounds(10, 46, 847, 571);
		pnlShowErrors.add(scpErrorList);
		
		errorTblModel = new errorListTableModel(new ArrayList<ShowError>());
		tblErrorList = new JTable(errorTblModel);
		scpErrorList.setViewportView(tblErrorList);
		
		JComboBox cbxShowsWithErrors = new JComboBox(new DefaultComboBoxModel<>());
		cbxShowsWithErrors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<ShowError> errors = null;
				
				String selected = (String) ((JComboBox)e.getSource()).getSelectedItem();
				
				if(!selected.equals("Selecteer")) {
					if(selected.equals("All Errors")) {
						errors = DAOerrors.getAllShowErrors();
						errors.addAll(DAOerrors.getAllShowErrors(true));
					} else {
						errors = DAOerrors.getShowErrorsByShowName(selected);
					}
				}
				
				if(errors != null) {
					errorTblModel = new errorListTableModel(errors);
					tblErrorList = new JTable(errorTblModel);
					scpErrorList.setViewportView(tblErrorList);
					
					errorTblModel.showFinished(chckbxShowChecked.isSelected());
				}
			}
		});
		cbxShowsWithErrors.setBounds(10, 12, 285, 23);
		
		cbxShowsWithErrors.addItem("Selecteer");
		cbxShowsWithErrors.addItem("All Errors");
		for(String s : DAOerrors.getAllShowNamesWithErrors())
			cbxShowsWithErrors.addItem(s);
		
		pnlShowErrors.add(cbxShowsWithErrors);
		
		chckbxShowChecked = new JCheckBox("Show Checked");
		chckbxShowChecked.setSelected(false);
		chckbxShowChecked.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorTblModel.showFinished(((JCheckBox)e.getSource()).isSelected());
			}
		});
		chckbxShowChecked.setBounds(302, 12, 111, 23);
		pnlShowErrors.add(chckbxShowChecked);
		
		JButton btnDownloadSelectedErrors = new JButton("Download Selected");
		btnDownloadSelectedErrors.setBounds(604, 12, 253, 23);
		pnlShowErrors.add(btnDownloadSelectedErrors);
		
		JButton btnSetSelectedChecked = new JButton("Set Selected Checked");
		btnSetSelectedChecked.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRows[] = tblErrorList.getSelectedRows();
				
				for(int x = 0; x < selectedRows.length; x++) {
					ShowError me = errorTblModel.getRow(x);
					me.setChecked(true);
					DAOerrors.saveError(me);
					
					errorTblModel.showFinished(chckbxShowChecked.isSelected());
				}				
			}
		});
		btnSetSelectedChecked.setBounds(419, 12, 161, 23);
		pnlShowErrors.add(btnSetSelectedChecked);

		JPanel pnlSettings = new JPanel();
		tpnProgram.addTab("Settings", null, pnlSettings, null);
		pnlSettings.setLayout(null);
		
		txtTorrentExe = new JTextField();
		txtTorrentExe.setText(settings.getUtorrentPath());
		txtTorrentExe.setBounds(186, 11, 391, 20);
		pnlSettings.add(txtTorrentExe);
		txtTorrentExe.setColumns(10);
		
		JLabel lblUtorrentExecutable = new JLabel("UTorrent Executable:");
		lblUtorrentExecutable.setBounds(10, 11, 151, 20);
		pnlSettings.add(lblUtorrentExecutable);
		
		JButton btnSave = new JButton("SAVE");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settings.setUtorrentPath(txtTorrentExe.getText());
			}
		});
		btnSave.setBounds(0, 58, 867, 570);
		pnlSettings.add(btnSave);
		
		/*JLabel lblUpdateInterval = new JLabel("Update Interval:");
		lblUpdateInterval.setBounds(10, 43, 108, 20);
		pnlSettings.add(lblUpdateInterval);
		*/
		
		this.frmMain.setVisible(true);
	}
	
	public void delShow(int index) {
		watch me = watchedTblModel.removeRow(index);
		cbxShowSelect.removeItem(me);

		DAOwatch.removeWatch(me);
		
		setAllShowscount(getAllShowscount()-1);
		
		if(!me.hasEnded())
			setRunningShows(getRunningShows()-1);
		
		lblShowcount.setText("Number of shows: " + allShowscount + " (Running: " + runningShows + ")");
		repaint();
	}
	
	public void addShow(String showName, int id) {
		if(showName.length() > 4)
			if (showName.substring(0, 4).toLowerCase().equals("the "))
				showName = showName.substring(4) + ", The";

		watch me = watchedTblModel.addRow(showName, id);

		if (me != null) {
			cbxShowSelect.addItem(me);

			DAOwatch.saveWatch(me);
			
			setAllShowscount(getAllShowscount()+1);
			
			if(!me.hasEnded())
				setRunningShows(getRunningShows()+1);
			
			lblShowcount.setText("Number of shows: " + allShowscount + " (Running: " + runningShows + ")");
			repaint();
		}
	}

	public watchedTableModel getWatchedTblModel() {
		return watchedTblModel;
	}

	public void repaint() {
		frmMain.repaint();
	}

	public int getRunningShows() {
		return runningShows;
	}

	public void setRunningShows(int runningShows) {
		this.runningShows = runningShows;
	}

	public int getAllShowscount() {
		return allShowscount;
	}

	public void setAllShowscount(int allShowscount) {
		this.allShowscount = allShowscount;
	}
}
