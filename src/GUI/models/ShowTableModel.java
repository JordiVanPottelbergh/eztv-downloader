package GUI.models;

import java.util.TreeMap;

import javax.swing.table.AbstractTableModel;

public class ShowTableModel extends AbstractTableModel {
private static final long serialVersionUID = 1L;
	
	private TreeMap<Integer, String> list = new TreeMap<>();
	private String[] columnNames = new String[] {"Show ID", "Show"};
	
	public ShowTableModel(TreeMap<Integer, String> list) {
		this.list = list;
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return list.keySet().toArray()[rowIndex];
			
			case 1:
				return list.get(list.keySet().toArray()[rowIndex]);
			
			default:
				return null;
		}
	}
}
