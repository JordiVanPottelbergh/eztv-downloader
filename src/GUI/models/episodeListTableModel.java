package GUI.models;

import java.util.NavigableMap;
import java.util.TreeMap;

import javax.swing.table.AbstractTableModel;

public class episodeListTableModel extends AbstractTableModel {
private static final long serialVersionUID = 1L;
	
	private NavigableMap<String, String> list = new TreeMap<>();
	private String[] columnNames = new String[] {"Episode Number", "Link"};
	
	public episodeListTableModel(TreeMap<String, String> list) {
		this.list = list.descendingMap();
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	public String getUrlAtRow(int row) {
		return (String) getValueAt(row, 1);
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return list.keySet().toArray()[rowIndex];
			
			case 1:
				return list.get(list.keySet().toArray()[rowIndex]);
			
			default:
				return null;
		}
	}
	
}
