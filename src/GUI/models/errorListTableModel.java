package GUI.models;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import Classes.ShowError;

public class errorListTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;

	private ArrayList<ShowError> list = new ArrayList<>();
	private ArrayList<ShowError> fullList = new ArrayList<>();
	
	private String[] columnNames = new String[] { "Show Name", "Episode Name", "Magnet URI" };

	public errorListTableModel(ArrayList<ShowError> errors) {
		this.list = errors;
		fullList = errors;
	}

	public ShowError getRow(int row) {
		return list.get(row);
	}
	
	public void showFinished(boolean checkbox) {
		if (checkbox) {
			list = fullList;
		} else {
			ArrayList<ShowError> finished = new ArrayList<>();

			for (ShowError me : fullList) {
				if (!me.isChecked())
					finished.add(me);
			}
			
			list = finished;
		}

		this.fireTableDataChanged();
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return ((ShowError) list.toArray()[rowIndex]).getShowName();
	
			case 1:
				return ((ShowError) list.toArray()[rowIndex]).getEpisodeName();
				
			case 2:
				return ((ShowError) list.toArray()[rowIndex]).getMagnetURI();
	
			default:
				return null;
		}
	}
}