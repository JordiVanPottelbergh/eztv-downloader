package GUI.models;

import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.table.AbstractTableModel;

import database.DAOwatch;

import webCrawler.crawlWeb;

import Classes.watch;
import GUI.AlertWindow;

@SuppressWarnings("rawtypes")
public class watchedTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	
	private TreeSet<watch> watchlist = new TreeSet<>();
	private TreeSet<watch> fullList = new TreeSet<>();
	private String[] columnNames = new String[] {"Name", "Last Episode Number", "Url", "Ended"};
	
	public watchedTableModel(TreeSet<watch> watchlist) {
		this.watchlist = watchlist;
		this.fullList = watchlist;
	}
	
	public void showFinished(boolean checkbox) {
		if (checkbox) {
			watchlist = fullList;
		} else {
			TreeSet<watch> finished = new TreeSet<>();

			for (watch me : fullList) {
				if (!me.hasEnded())
					finished.add(me);
			}

			watchlist = finished;
		}

		this.fireTableDataChanged();
	}

	public watch addRow(String name, int id) {
		boolean hasEnded = false;
		TreeMap tmp = crawlWeb.getLastEpisode(watch.getBase() + id + "/", 1);
		
		watch me = null;
		
		if(tmp != null) {
			if(tmp.firstKey().equals("ENDED"))
				hasEnded = true;
			
			TreeSet<watch> results = DAOwatch.getWatch(-1, name, "");

			if (results.size() <= 0) {
				
				if(hasEnded) {
					me = new watch(-1, name, id, (String) tmp.keySet().toArray()[1]);
					me.setHasEnded(true);
				} else {
					me = new watch(-1, name, id, (String) tmp.keySet().toArray()[0]);
				}
				
				watchlist.add(me);
				this.fireTableRowsInserted(0, getColumnCount());
			}

		} else {
			new AlertWindow("Error: there seem to be no results for " + name);
		}
		
		return me;
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return watchlist.size();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		/*if(((watch)watchlist.toArray()[rowIndex]).hasEnded())
			return null;*/
		
		switch(columnIndex) {
			case 0:
				return ((watch) watchlist.toArray()[rowIndex]).getName();
			
			case 1:
				return ((watch) watchlist.toArray()[rowIndex]).getEpisodeNumber();
			
			case 2:
				return ((watch) watchlist.toArray()[rowIndex]).getUrl();
			
			case 3:
				return (((watch) watchlist.toArray()[rowIndex]).hasEnded() ? "Yes" : "No");
				
			default:
				return null;
		}
	}

	public watch getRow(int i) {
		return (watch) watchlist.toArray()[i];
	}

	public watch removeRow(int i) {
		watch retMe = getRow(i);
		watchlist.remove(retMe);
		this.fireTableRowsDeleted(0, getColumnCount());
		
		return retMe;
	}

	public void rowsUpdated() {
		this.fireTableRowsUpdated(0, getColumnCount());		
	}
	
}
