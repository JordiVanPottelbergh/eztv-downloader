package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Classes.ShowError;

public class DAOerrors {
	
	static Logger lgr = Logger.getLogger(DAOwatch.class.getName());
	
	public static void saveError(ShowError me) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		
		try {
			/*
			 * 	showName VARCHAR(100),
		     *  episodeName VARCHAR(100),
		     *  magnetURI VARCHAR(500),
		     *  checked INTEGER(1)
			 */
			
			if(getShowError(me.getEpisodeName()) == null) {
				ps = con.prepareStatement("INSERT INTO errors(showName, episodeName, magnetURI, checked) values (?,?,?,?)");
			} else {
				ps = con.prepareStatement("UPDATE errors SET showName = ?, episodeName = ?, magnetURI = ?, checked = ? WHERE episodeName = ?");
				ps.setString(5, me.getEpisodeName());
			}
			
			ps.setString(1, me.getShowName());
			ps.setString(2, me.getEpisodeName());
			ps.setString(3, me.getMagnetURI());
			ps.setBoolean(4, me.isChecked());
			
			ps.executeUpdate();
			
			
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

            try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
			
		} finally {
			try {
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
	}
	
	public static ArrayList<String> getAllShowNamesWithErrors() {
		ArrayList<String> showNames = new ArrayList<>();
		
		Connection con = DbConnect.getConnection();

		Statement st = null;
		ResultSet rs = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery("SELECT DISTINCT(showName) FROM errors");

			if (rs.next())
				showNames.add(rs.getString(1));
			
		} catch (SQLException ex) {
			try {
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			ex.printStackTrace();

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return showNames;
	}
	
	public static ArrayList<ShowError> getAllShowErrors() {
		return getAllShowErrors(false);
	}
	
	public static ArrayList<ShowError> getShowErrorsByShowName(String showName) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<ShowError> toRet = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM errors WHERE showName = ?";
			
			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setString(1, showName);
			
			rs = ps.executeQuery();
			
		    while(rs.next()) {
		    	ShowError me = new ShowError(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBoolean(4));
		    	
		    	toRet.add(me);
		    }
		    
		    return toRet;
		    
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            
			try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
            
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
		
		return toRet; 
	}
	
	public static ShowError getShowError(String episodeName) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
				
		try {
			String sql = "SELECT * FROM errors WHERE episodeName = ?";
			
			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setString(1, episodeName);
			
			rs = ps.executeQuery();
			
		    if(rs.next())
		    	return new ShowError(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBoolean(4));
		    	  
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            
			try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
            
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
		
		return null; 
	}
	
	public static ArrayList<ShowError> getAllShowErrors(boolean checked) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<ShowError> toRet = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM errors WHERE checked = ?";
			
			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setBoolean(1, checked);
			
			rs = ps.executeQuery();
			
		    while(rs.next()) {
		    	ShowError me = new ShowError(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBoolean(4));
		    	
		    	toRet.add(me);
		    }
		    
		    return toRet;
		    
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            
			try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
            
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
		
		return toRet; 
	}
	
}
