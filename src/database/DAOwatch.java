package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import Classes.watch;

public class DAOwatch {
	static Logger lgr = Logger.getLogger(DAOwatch.class.getName());
	
	public static void removeWatch(watch me) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "DELETE FROM watching WHERE showId = ?";
			
			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setInt(1, me.getId());
			
			ps.executeUpdate();
			
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

            try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
			
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
	}
	
	public static void saveWatch(watch me) {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "";
			
			/*
			 * 	showId INTEGER PRIMARY KEY AUTOINCREMENT ,
			 *  showName varchar(50) NOT NULL,
			 *	siteId number(4) NOT NULL,
			 *	episodeNumber varchar(50) NOT NULL,
			 *  hasEnded INTEGER(1)
			 */
			
			if(me.getId() == -1) {
				sql = "INSERT INTO watching(showName, siteId, episodeNumber, hasEnded) VALUES(?,?,?,?)";
				
				ps = (PreparedStatement) con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			} else {
				sql = "UPDATE watching SET showName=?, siteId=?, episodeNumber=?, hasEnded=? WHERE showId=?";
				
				ps = (PreparedStatement) con.prepareStatement(sql);
				ps.setInt(5, me.getId());
			}
			
			ps.setString(1, me.getName());
			ps.setInt(2, me.getSiteId());
			ps.setString(3, me.getEpisodeNumber());
			ps.setBoolean(4, me.hasEnded());
			
			ps.executeUpdate();
			
			if(me.getId() == -1) {
				rs = ps.getGeneratedKeys();
				
				if(rs.next())
					me.setId(rs.getInt(1));
			}
			
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

            try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
			
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
	}
	
	public static TreeSet<watch> getWatch(int id, String name, String episodeNumber) {
		Connection con = DbConnect.getConnection();

		TreeSet<watch> watching = new TreeSet<>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM watching WHERE showName LIKE ? AND episodeNumber LIKE ?";
			
			if(id != -1)
				sql += " AND showId = ?";
			
			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setString(1, "%"+name+"%");
			ps.setString(2, "%"+episodeNumber+"%");
			
			if(id != -1)
				ps.setInt(3, id);
			
			rs = ps.executeQuery();
			
		    while(rs.next()) {
		    	watch me = new watch(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
		    	me.setHasEnded(rs.getBoolean(5));
		    	
		    	watching.add(me);
		    }
		    
		    return watching;
		    
		} catch(SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            
			try {
                if(con != null) {
                    con.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
            
		} finally {
			try {
                if(rs != null) {
                    rs.close();
                }
                if(ps != null) {
                    ps.close();
                }
				
			} catch(SQLException e) {
	            lgr.log(Level.WARNING, e.getMessage(), e);
	        }
		}
		
		return watching;
	}
}
