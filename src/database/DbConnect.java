package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class om database connectie te leggen en connectie te returnen
 * @author jaggy
 */
public class DbConnect {
	private static Connection conn = null;
	
    private static String url = "jdbc:sqlite:test.db";
	
    /**
     * Functie zal een werkende connectie returnen
     * @return java.sql.Connection
     */
	public static Connection getConnection() {
		try {
			if(conn == null || conn.isClosed()) {
				conn = DriverManager.getConnection(url);
			}
            
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(DbConnect.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
		
		return conn;
	}
}
