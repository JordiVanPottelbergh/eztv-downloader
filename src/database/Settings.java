package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Settings {
	private String utorrentPath;
	private boolean showFinished;
	
	public Settings() {
		fillSettings();
	}

	public boolean showFinished() {
		return showFinished;
	}

	public String getUtorrentPath() {
		return utorrentPath;
	}

	public void setShowFinished(boolean showFinished) {
		this.showFinished = showFinished;
		saveSettings();
	}
	
	public void setUtorrentPath(String utorrentPath) {
		this.utorrentPath = utorrentPath;
		saveSettings();
	}

	private void fillSettings() {
		Connection con = DbConnect.getConnection();

		Statement st = null;
		ResultSet rs = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery("select * from settings");

			if (rs.next()) {
				utorrentPath = rs.getString(1);
				showFinished = rs.getBoolean(2);
			}

		} catch (SQLException ex) {
			try {
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			ex.printStackTrace();

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void saveSettings() {
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("update settings set exepath = ?, showFinished = ?");
			
			ps.setString(1, utorrentPath);
			ps.setBoolean(2, showFinished);
			
			ps.executeUpdate();
			
		} catch (SQLException ex) {
			try {
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			ex.printStackTrace();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
}
