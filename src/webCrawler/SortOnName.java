package webCrawler;

import java.util.Comparator;
import java.util.HashMap;

public class SortOnName implements Comparator<Integer> {
	
	private HashMap<Integer, String> base;

	public SortOnName(HashMap<Integer,String> base) {
		this.base = base;
	}
	
	@Override
	public int compare(Integer o1, Integer o2) {
		return base.get(o1).toLowerCase().compareTo(base.get(o2).toLowerCase()); 
	}

}
