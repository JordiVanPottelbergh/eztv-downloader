package webCrawler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import database.DAOerrors;

import Classes.ShowError;
import GUI.AlertWindow;

public class crawlWeb {
	public static HashMap<String,String> getShowInfo(String url) {
		HashMap<String,String> retMe = new HashMap<>();
		
		try {
			URL webPage = new URL(url);
			BufferedReader in = new BufferedReader(new InputStreamReader(webPage.openStream()));
			
			String inputLine;
			
			Pattern banner = Pattern.compile("^.*?class=\"show_info_banner_logo\"><img src=\"(.*?)\".*?$");
			Pattern picture = Pattern.compile("^.*?class=\"show_info_main_logo\"><img src=\"(.*?)\".*?$");
			Pattern info_start = Pattern.compile("^.*?<td rowspan=\"\\d+\" valign=\"top\" style=\"padding:.*?$");
			Pattern airfo_start = Pattern.compile("^.*?show_info_airs_status\" height=\"\\d+\">");//<td class="show_info_airs_status" height="84">
			Pattern info_stop = Pattern.compile("^.*?<\\/td>.*?$");
			
			boolean capture = false;
			String cap_data = "";
			
			while ((inputLine = in.readLine()) != null) {
				Matcher m_banner = banner.matcher(inputLine);
				Matcher m_picture = picture.matcher(inputLine);
				Matcher m_info_start = info_start.matcher(inputLine);
				Matcher m_info_stop = info_stop.matcher(inputLine);
				Matcher m_airfo_start = airfo_start.matcher(inputLine);
				
				if(m_info_stop.matches())
					capture = false;
				
				if(capture)
					cap_data += inputLine;
					
				if(m_banner.matches())
					retMe.put("banner", (m_banner.group(1).startsWith("http:") ? "" : "http:") + m_banner.group(1));
				
				if(m_picture.matches())
					retMe.put("pic", (m_picture.group(1).startsWith("http:") ? "" : "http:") + m_picture.group(1));
				
				if(m_info_start.matches()) {
					cap_data += "<br\\><br\\>";
					capture = true;
				}
				
				if(m_airfo_start.matches())
					capture = true;
			}
			
			in.close();
			
			retMe.put("text", cap_data);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return retMe;
	}
	
	public static TreeMap<Integer, String> getAllShows() {
		try {
			URL webPage = new URL("http://eztv.it/");
			BufferedReader in = new BufferedReader(new InputStreamReader(webPage.openStream()));

			String inputLine;
			TreeMap<Integer, String> data = new TreeMap<>();
			
			Pattern allShows = Pattern.compile("^.*?<option value=\"\"> -- select show -- </option>(.*?)$");
			Pattern singleShow = Pattern.compile("^value=\"(\\d{1,4})\">(.*?)<.*?$");
			
			while ((inputLine = in.readLine()) != null) {
				Matcher m_allShows = allShows.matcher(inputLine);
				
				if(m_allShows.matches()) {
					for(String s : m_allShows.group(1).split("<option ")) {
						Matcher m_singleShow = singleShow.matcher(s);
						
						if(m_singleShow.matches()) {
							data.put(Integer.parseInt(m_singleShow.group(1)), m_singleShow.group(2));
						}
					}
				}
			}
			in.close();
		
			return data;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int[] parseEpisode(String episodeName) {
		Pattern episodeSE = Pattern.compile("^.*?\\.S(\\d{1,2})E(\\d{1,2})\\..*?$");//S01E01 FORMAT
		Pattern episodeX = Pattern.compile("^.*?\\.(\\d{1,2})x(\\d{1,2})\\..*?$");  //01x01 FORMAT
		Pattern episodeOF = Pattern.compile("^.*?\\.(\\d{1,2})of(\\d{1,2})\\..*?$");//01of01 FORMAT
		
		Pattern episodeSEE = Pattern.compile("^.*?\\.S(\\d{1,2})E(\\d{1,2})E(\\d{1,2})\\..*?$");//Multiple Episodes in one: S01E01E02 FORMAT
		
		//Special (FUCK YOU) Top Gear format
		//Pattern episodeTG = Pattern.compile("^.*?\\.S(\\d{1,2})\\.Part(\\d{1,2})\\..*?$");//S12.Part3 FORMAT
		
		/*
		 * KEYS WILL BE
		 * 01x01
		 * 01x02
		 */
		
		Matcher m_episodeSE = episodeSE.matcher(episodeName.toUpperCase());
		
		int episode = -1;
		int season = -1;
		
		if(m_episodeSE.matches()) {
			episode = Integer.parseInt(m_episodeSE.group(2));
			season = Integer.parseInt(m_episodeSE.group(1));
			
		} else {//Assuming if it's not S00E00 format, then it will be the 00x00 format
		
			Matcher m_episodeX = episodeX.matcher(episodeName);
			
			if(m_episodeX.matches()) {
				episode = Integer.parseInt(m_episodeX.group(2));
				season = Integer.parseInt(m_episodeX.group(1));
			} else {
				Matcher m_episodeOF = episodeOF.matcher(episodeName.toLowerCase());
				
				if(m_episodeOF.matches()) {
					episode = Integer.parseInt(m_episodeOF.group(2));
					season = Integer.parseInt(m_episodeOF.group(1));
				} else {
					Matcher m_episodeSEE = episodeSEE.matcher(episodeName.toUpperCase());
					
					if(m_episodeSEE.matches()) {//We will just take the last of the 2 eps & set that as a number,...
						episode = Integer.parseInt(m_episodeSEE.group(3));
						season = Integer.parseInt(m_episodeSEE.group(1));
					}
				}
			}			
		}
		
		/*if(episode == -1) {
			throw new EpisodeFormatException("[ERROR] whilst matching " + episodeName);
		}*/
		
		return new int[] {episode, season};
	}
	
	private static void handleUnmatchable(TreeMap<String, String> unhandledEpisodes, String showName) {
		String totalErrorMsg = "Error while handling " + showName +
				"\nCouldn't handle the following episodes because of an unknown episodeFormat:\n\n";
		
		for(String s : unhandledEpisodes.keySet()) {
			ShowError me = new ShowError(showName, s, unhandledEpisodes.get(s), false);
			
			if(DAOerrors.getShowError(s) == null) {
				DAOerrors.saveError(me);
			
				totalErrorMsg += s + "\n";
			}
		}
		
		if (!totalErrorMsg.equals("Error while handling " + showName + "\nCouldn't handle the following episodes because of an unknown episodeFormat:\n\n"))
			new AlertWindow(totalErrorMsg);
	}
	
	public static TreeMap<String, String> getLastEpisode(String episodeUrl, int limit) {
		try {
			URL webPage = new URL(episodeUrl);
			BufferedReader in = new BufferedReader(new InputStreamReader(webPage.openStream()));
			
			String inputLine;
			String showName = "";
			
			TreeMap<String, String> data = new TreeMap<>(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					if(o1.equals("ENDED") || o2.equals("ENDED"))
						return 1;
					
					int o1Arr[] = new int[2];
					int o2Arr[] = new int[2];
					
					int x = 0;
									
					for(String s : o1.split("x")) {
						o1Arr[x] = Integer.parseInt(s);
						x++;
					}
					
					x = 0;
					for(String s : o2.split("x")) {
						o2Arr[x] = Integer.parseInt(s);
						x++;
					}

					if(o1Arr[0] > o2Arr[0])
						return -1;
						
					if(o1Arr[0] < o2Arr[0])
						return 1;
					
					//1x1 == 1x1
					if(o1Arr[0] == o2Arr[0]) {
						if(o1Arr[1] == o2Arr[1])
							return 0;
						
						if(o1Arr[1] > o2Arr[1])
							return -1;
						
						if(o1Arr[1] < o2Arr[1])
							return 1;
						
					}
															
					return 0;
				}
			});
			
			TreeMap<String, String> unhandledEpisodes = new TreeMap<>();
			
			Pattern magnetUri = Pattern.compile("^.*?href=\"magnet:\\?(.*?)dn=(.*?)&tr(.*?)\".*?$");
			Pattern ended = Pattern.compile("^.*?Status: <b>Ended<\\/b>.*?$");
			Pattern showname = Pattern.compile("^.*?\"section_post_header\">Show Information: <b>(.*?)<\\/b>.*?$");
			
			while ((inputLine = in.readLine()) != null) {
				Matcher m_magnetUri = magnetUri.matcher(inputLine);
				Matcher m_ended = ended.matcher(inputLine);
				Matcher m_showname = showname.matcher(inputLine);
				
				if(m_showname.matches())
					showName = m_showname.group(1);
				
				if(m_ended.matches())
					data.put("ENDED", "ENDED");
				
				if(m_magnetUri.matches()) {
					//System.out.println(episodeUrl + " => " + m_magnetUri.group(2));
					
					int epNrs[] = parseEpisode(m_magnetUri.group(2));
					
					int episode = epNrs[0];
					int season = epNrs[1];
					
					if(episode == -1) {
						unhandledEpisodes.put(m_magnetUri.group(2), "magnet:?" + m_magnetUri.group(1) + "dn=" + m_magnetUri.group(2) + "&tr" + m_magnetUri.group(3));
						
						continue;
					}
					
					data.put((season <= 9 ? "0" : "") + season + "x" + (episode <= 9 ? "0" : "") + episode, "magnet:?" + m_magnetUri.group(1) + "dn=" + m_magnetUri.group(2) + "&tr" + m_magnetUri.group(3));
					
					int count = data.size();
					if(m_ended.matches())
						count--;
					
					if(count == limit) {
						in.close();
						
						if(unhandledEpisodes.size() > 0)
							handleUnmatchable(unhandledEpisodes, showName);
						
						return data;
					}
				}
			}
			
			if(unhandledEpisodes.size() > 0)
				handleUnmatchable(unhandledEpisodes, showName);
			
			if(data.size() <= 0)
				return null;
			
			
			return data;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if(e.getMessage().startsWith("Server returned HTTP response code: 503"))
				System.out.println("eztv is offline");
			e.printStackTrace();
		}
		return null;
	}
	
	public static TreeMap<String, String> getEpisodeMagnetList(String episodeUrl) {
		try {
			URL webPage = new URL(episodeUrl);
			BufferedReader in = new BufferedReader(new InputStreamReader(webPage.openStream()));

			String inputLine;
			TreeMap<String, String> data = new TreeMap<>();
			
			Pattern magnetUri = Pattern.compile("^.*?href=\"magnet:\\?(.*?)dn=(.*?)&tr(.*?)\".*?$");
						
			while ((inputLine = in.readLine()) != null) {
				Matcher m_magnetUri = magnetUri.matcher(inputLine);
				
				if(m_magnetUri.matches()) {
					int epNrs[] = parseEpisode(m_magnetUri.group(2));
					
					int episode = epNrs[0];
					int season = epNrs[1];
					
					if(episode == -1)
						continue;
					
					data.put((season <= 9 ? "0" : "") + season + "x" + (episode <= 9 ? "0" : "") + episode, "magnet:?" + m_magnetUri.group(1) + "dn=" + m_magnetUri.group(2) + "&tr" + m_magnetUri.group(3));
				}
			}
			in.close();
		
			return data;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static TreeMap<Integer, String> getShowBySearch(String showName) {
		HashMap<Integer, String> test = new HashMap<>();
		TreeMap<Integer, String> retMe = new TreeMap<Integer, String>(new SortOnName(test));
		
		Pattern showPattern = Pattern.compile("^.*?<a href=\"\\/shows\\/(\\d{1,3}).*? Description about (.+)\"><\\/a>.*?$");
		
		try {
			String urlParameters = "SearchString1=" + showName + "&SearchString=&search=Search";
			String request = "http://eztv.it/search/";
			URL url = new URL(request); 
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false); 
			connection.setRequestMethod("POST"); 
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches (false);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
						
			while ((inputLine = in.readLine()) != null) {
				Matcher m_showPattern = showPattern.matcher(inputLine);
				
				if(m_showPattern.matches()) {
					test.put(Integer.parseInt(m_showPattern.group(1)), m_showPattern.group(2));
				}
				
				
			}
			
			in.close();
			connection.disconnect();
			
			retMe.putAll(test);
			return retMe;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
